import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostPagesComponent } from './post-pages.component';

describe('PostPagesComponent', () => {
  let component: PostPagesComponent;
  let fixture: ComponentFixture<PostPagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostPagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
