import { Component, OnInit } from '@angular/core';
import {AppService} from '../app.service';
import {Subscription, Observable} from 'rxjs';

@Component({
  selector: 'app-post-pages',
  templateUrl: './post-pages.component.html',
  styleUrls: ['./post-pages.component.css'],
  providers: [AppService]
})
export class PostPagesComponent implements OnInit {

  subscription: Subscription = null;
  posts: Array<any>;
  constructor(private _httpService: AppService) { }

  ngOnInit() {
    this.getData();
  }

  public getData() {
    this.subscription = this._httpService.getMethod('https://jsonplaceholder.typicode.com/posts')
    .subscribe (
      data => {
        this.posts = data;
      },
      error => {
        this.posts = [];
      });
  }

}
