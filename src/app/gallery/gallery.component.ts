import { Component, OnInit } from '@angular/core';
import {AppService} from '../app.service';
import {Subscription, Observable} from 'rxjs';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css'],
  providers: [AppService]
})
export class GalleryComponent implements OnInit {

  subscription: Subscription = null;
  posts: Array<any>;
  constructor(private _httpService: AppService) { }

  ngOnInit() {
    this.getData();
  }

  public getData() {
    this.subscription = this._httpService.getMethod('https://jsonplaceholder.typicode.com/photos')
    .subscribe (
      data => {
        this.posts = data;
      },
      error => {
        this.posts = [];
      });
  }

}
