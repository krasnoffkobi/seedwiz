import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  isSubmited: boolean;
  txtFirstName: string;
  txtLastName: string;
  txtComments: string;
  constructor() { }

  ngOnInit() {
    this.isSubmited = false;
  }

  Submit() {
    this.isSubmited = true;
  }

  isnullOrEmpty(str: string): boolean {
    if (typeof str !== 'undefined' && str && str.trim() !== '') {
      return true;
    } else {
      return false;
    }
  }
}
