import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { map } from 'rxjs/operators';


@Injectable()
export class AppService {

  constructor (private _http: Http) {}

  getMethod(url: string) {
    return this._http.get(url)
        .pipe(map(res => res.json()));
  }

}
